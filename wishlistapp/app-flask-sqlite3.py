from flask import Flask, render_template, request, redirect, url_for, session
import sqlite3 as lite

app = Flask(__name__)

@app.route("/")
def main():
    if 'username' in session:
        username_session = session['username']
        return render_template('index_login.html', session_user_name=username_session)
    else:
        return render_template('index.html')

@app.route("/pesan")
def page_pesan():
    return "Hello Flask 101!. Ini ditulis dari url /pesan"

@app.route('/hello/<username>')
def hello_user(username):
    return 'Hello %s' % username

@app.route('/showSignIn')
def showSignIn():
    return render_template('signin_sq.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None

    if request.method == 'POST':
        session_username = request.form['username']
        session_password = request.form['password']

        con = lite.connect("app1.db")
        cur = con.cursor()
        cur.execute("SELECT COUNT(1) FROM Users WHERE username=?", [session_username]) # CHECKS IF USERNAME EXSIST
        if cur.fetchone()[0]:
            cur.execute("SELECT password FROM Users WHERE password=?", [session_password]) # FETCH THE HASHED PASSWORD
            for row in cur.fetchall():
                if session_password == row[0]:
                    session['username'] = request.form['username']
                    return redirect(url_for('main'))
            else:
                error = "Invalid Credential username"
        else:
            error = "Invalid Credential password"
    return render_template('signin_sq.html', error=error)


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('main'))

@app.route('/showSignUp')
def showSignUp():
    return render_template('signup.html')

@app.route('/saveSignUp', methods=['GET', 'POST'])
def saveSignUp():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        gender = request.form['gender']

        con = lite.connect('app1.db')

        with con:
            cur = con.cursor()
            cur.execute("INSERT INTO Users VALUES (?,?,?,?,?,?)",(username,password,first_name,last_name,email,gender))
            con.commit()

    return redirect(url_for('userlist'))
#Masih error :(

@app.route('/userlist')
def userlist():
    con = lite.connect("app1.db")
    con.row_factory = lite.Row

    cur = con.cursor()
    cur.execute("select * from Users")

    rows = cur.fetchall()
    con.close()
    return render_template("user_list.html",rows = rows)

@app.route('/carlist')
def carlist():
    con = lite.connect("app1.db")
    con.row_factory = lite.Row

    cur = con.cursor()
    cur.execute("select * from Cars")

    rows = cur.fetchall();
    con.close()
    return render_template("carlist-button.html",rows = rows)

@app.route('/insertcar')
def insertcar():
    return render_template("insertcar.html")

@app.route('/savecar', methods=['GET', 'POST'])
def savecar():
    if request.method == 'POST':
        idcar = request.form['idcar']
        namecar = request.form['namecar']
        pricecar = request.form['pricecar']

        con = lite.connect('app1.db')

        with con:
            cur = con.cursor()
            cur.execute("INSERT INTO Cars VALUES (?,?,?)",(idcar,namecar,pricecar))
            con.commit()

    return redirect(url_for('carlist'))

@app.route('/updatecar')
def updatecar():
    con = lite.connect('app1.db')
    con.row_factory = lite.Row

    cur = con.cursor()
    cur.execute("select * from Cars")

    rows = cur.fetchall()
    con.close()
    return render_template("updatecar.html", rows = rows)

@app.route('/saveupdatecar', methods=['GET', 'POST'])
def saveupdatecar():
    if request.method == 'POST':
        idcar = request.form['idcar']
        namecar = request.form['namecar']
        pricecar = request.form['pricecar']
        idselect = request.form['idselect']

        con = lite.connect('app1.db')
        cur = con.cursor()

        with con:
            cur.execute("UPDATE Cars SET Id=?, Name=?, Price=? WHERE Id=?", (idcar, namecar, pricecar, idselect))
            con.commit()

    return redirect(url_for('carlist'))

@app.route('/carlist_delete/<int:idcar>', methods=['GET', 'POST'])
def carlist_delete(idcar):
    try:
        idcar = str(idcar)
        con = lite.connect('app1.db')
        cur = con.cursor()

        with con:
            cur.execute("DELETE FROM Cars WHERE Id=(?)",[idcar])
            con.commit()
        return redirect(url_for('carlist'))
    except:
        return redirect(url_for('carlist'))

@app.route('/carlist_edit/<int:idcar>')
def carlist_edit(idcar):
    idcar = str(idcar)
    con = lite.connect('app1.db')
    con.row_factory = lite.Row

    cur = con.cursor()
    cur.execute("SELECT * from Cars where Id=?", [idcar])
    rows = cur.fetchall()
    con.close()
    return render_template("edit-carlist.html", rows = rows)

@app.route('/save_edit_carlist', methods=['GET', 'POST'])
def save_edit_carlist():
    if request.method == 'POST':
        idcar = request.form['idcar']
        namecar = request.form['namecar']
        pricecar = request.form['pricecar']
        idselect = request.form['idselect']

        con = lite.connect('app1.db')
        cur = con.cursor()

        with con:
            cur.execute("UPDATE Cars SET Id=?, Name=?, Price=? WHERE Id=?", (idcar, namecar, pricecar, idselect))
            con.commit()

    return redirect(url_for('carlist'))

app.secret_key = 'b4k$o_bv@k4r'

if __name__ == '__main__':
    app.run(debug="True")
