CREATE TABLE `users` (
  `id` int(11) DEFAULT NULL, AUTO_INCREMENT,
  `username` varchar(20) NULL,
  `password` varchar(11) NULL,
  `first_name` varchar(20) NULL,
  `last_name` varchar(20) NULL,
  `email` varchar(35) NULL,
  `gender` varchar(6) NULL,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `gender`) VALUES
(1, 'admin', 'AciDJsSB0YH', 'Patricia', 'Ramirez', 'pramirez0@ed.gov', 'Female'),
(2, 'dhanson1', 'zSzpZ8', 'Dennis', 'Hanson', 'dhanson1@jimdo.com', 'Male'),
(3, 'hbanks2', 'Cy5HkmXvl', 'Howard', 'Banks', 'hbanks2@mac.com', 'Male'),
(4, 'cholmes3', '7CUmCMk', 'Catherine', 'Holmes', 'cholmes3@wix.com', 'Female'),
(5, 'hlong4', 'Y6u5l5UdZbe', 'Howard', 'LongS', 'hlong4@narod.ru', 'Male'),
(6, 'ffields5', '2sIXMI8ZKfCc', 'Frank', 'Fields', 'ffields5@google.ca', 'Male'),
(7, 'jallen6', '4D50trZ', 'Juan', 'Allen', 'jallen6@army.mil', 'Male'),
(8, 'bkelley7', 'rV23qc5Woob', 'Bobby', 'Kelley', 'bkelley7@whitehouse.gov', 'Male'),
(9, 'jmarshall8', 'YiA6TEUoD', 'Joseph', 'Marshall', 'jmarshall8@dailymotion.com', 'Male'),
(10, 'hhughes9', 'TlYn8uTz', 'Heather', 'Hughes', 'hhughes9@goo.ne.jp', 'Female');
