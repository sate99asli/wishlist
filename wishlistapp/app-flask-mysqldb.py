from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '@kum4hfud'
app.config['MYSQL_DB'] = 'wishlist'
mysql = MySQL(app)

@app.route('/')
def index():
    if 'username' in session:
        username_session = session['username']
        if session['level_akses'] == "1":
            pengguna="admin"
            #return render_template('dashboard_admin.html', session_user_name=username_session, level_akses=session['level_akses'])
        elif session ['level_akses'] == "2":
            pengguna="user biasa"
            #return render_template('dashboard_user.html', session_user_name=username_session, level_akses=session['level_akses'])
        return render_template('layout.html', user=pengguna, username_session=username_session, level_akses=session['level_akses'])
    else:
        return render_template('index.html')

@app.route('/showLogin')
def showLogin():
    return render_template("login.html")

@app.route('/goLogin', methods=['GET', 'POST'])
def goLogin():
    error = ""
    level_akses_session = ""
    if request.method == 'POST':
        username_form = request.form['username']
        password_form = request.form['password']
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(1) FROM Users WHERE username = (%s)", [username_form])
        if cur.fetchone()[0]:
            cur.execute("SELECT password, level_akses FROM Users WHERE username = (%s)", [username_form])
            for row in cur.fetchall():
                if password_form == row[0]:
                    session['username'] = username_form
                    session['level_akses'] = str(row[1])
                    return redirect(url_for('index'))
                else:
                    error = "Invalid Credential - Error Password"
        else:
            error = "Invalid Credential - Error Username !!!"
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('username', None)
    session.pop('level_akses', None)

    return redirect(url_for('index'))
#login ok
#Then dashboard Menu
@app.route('/adminprofile')
def adminprofile():
    return render_template("profile_admin.html")

@app.route('/userprofile')
def userprofile():
    return render_template("profile_user.html")

@app.route('/makewish')
def makewish():
    return render_template("table_wish.html")
#dashboard

@app.route('/userlist')
def userlist():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Users")
    rows = cur.fetchall()
    return render_template('userlist_my.html', rows=rows)

@app.route('/showSignUp')
def showSignUp():
    return render_template('signup.html')

@app.route('/saveSignUp', methods=['GET', 'POST'])
def saveSignUp():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        gender = request.form['gender']

        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO Users VALUES (%s,%s,%s,%s,%s,%s)", (username,password,first_name,last_name,email,gender))
        mysql.connection.commit()

    return redirect(url_for('userlist'))

# look at saveSignUp, Is it true like this ?
# I use Id as Primary Key and set as auto increment on MySQL

@app.route('/carlist_view')
def carlist_view():
    username_session = session ['username']
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Cars")
    rows = cur.fetchall()
    return render_template("carlist_view.html", rows=rows, session_user_name=username_session, level_akses=session['level_akses'])

@app.route('/carlist')
def carlist():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Cars")
    rows = cur.fetchall()
#    return str(rows)
    return render_template('car_list_my.html', rows=rows)

@app.route('/insertcar')
def insertcar():
    return render_template("insertcar_my.html")

@app.route('/savecar', methods=['GET', 'POST'])
def savecar():
    if request.method == 'POST':
        idcar = request.form['idcar']
        namecar = request.form['namecar']
        pricecar = request.form['pricecar']

        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO Cars VALUES (%s,%s,%s)", (idcar,namecar,pricecar))
        mysql.connection.commit()

    return redirect(url_for('carlist'))

@app.route('/updatecar')
def updatecar():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Cars")
    rows = cur.fetchall()
    return render_template("updatecar_my.html", rows = rows)

@app.route('/saveupdatecar', methods=['GET', 'POST'])
def saveupdatecar():
    if request.method == 'POST':
        namecar = request.form['namecar']
        pricecar = request.form['pricecar']
        idselect = request.form['idselect']

        cur = mysql.connection.cursor()
        cur.execute("UPDATE Cars SET Name=%s, Price=%s WHERE Id=%s", (namecar, pricecar, idselect))
        mysql.connection.commit()

    return redirect(url_for('carlist'))

@app.route('/carlist_delete/<int:idcar>', methods=['GET', 'POST'])
def carlist_delete(idcar):
    try:
        idcar = str(idcar)
        cur = mysql.connection.cursor()
        cur.execute("DELETE FROM Cars WHERE Id=(%s)",[idcar])
        mysql.connection.commit()
        return redirect(url_for('carlist'))
    except:
        return redirect(url_for('carlist'))

app.secret_key = 'b4k$o_bv@k4r'

if __name__ == '__main__':
    app.run(debug=True)
