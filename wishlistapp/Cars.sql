CREATE TABLE `Cars` (
  `Id` int(11) DEFAULT NULL,
  `Name` text,
  `Price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `Cars` (`Id`, `Name`, `Price`) VALUES
(1, 'Audi', 52642),
(2, 'Mercedes', 57127),
(3, 'Skoda', 9000),
(4, 'Volvo', 29000),
(5, 'Bentley', 350000);
